JSON support only : 20090211 because Axiata Validation LIB

Tomcat:
Att context.xml Context >
 ```
 <Resource name="jdbc/USSD" 
 				auth="Container"
 				type="javax.sql.DataSource"
 				maxActive="100"
 				maxIdle="30"
 				maxWait="10000"
 				driverClassName="com.mysql.jdbc.Driver"
 				url="jdbc:mysql://localhost:3306/ussd"
 				username="root"
 				password="dialog@123"/>
 				```
## Conf
1.	Set the URL from USSD GW
2.	One time, need set callback url for configuration file. For that, please edit settings.xml (
		ni_ussd_url_prod : USSD GW URL Production
		ni_ussd_url_test : USSD GW URL Sandbox
		ussd_callback : ussd callback url that need to send to ussd GW with vxm
		ussd_cont : ussd callback url that need to send to ussd GW with vxm
		adaptorURL : adaptor  URL that need to append to USSD json body
		requestRouter : request router url, that will send all JSON request via this url if its there. [optional]
		requestRouterSkipPrefix 
3.	Add ussd port  to mifeussd.applications table
		a.	AM_ID – mife app id
		b.	Keyword – ussd shortcode (keep null for port only ussd apps, keep * for route all keywords)
		c.	Shortcode – ussd shortcode
		d.	Isunique – this is Boolean that represent port is unique or not
4.	Add URL, 3rd step id, keyword and shortcode to url_provision table

•	Please note, if need route #774# and #775*222#, need create two apps. One is unique, one is not unique 
