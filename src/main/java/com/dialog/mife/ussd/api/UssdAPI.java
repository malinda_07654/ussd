package com.dialog.mife.ussd.api;

import java.io.*;
import java.lang.reflect.Modifier;
import java.net.*;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;


import com.dialog.mife.ussd.dto.*;
import lk.dialog.ideabiz.library.JWT;
import lk.dialog.ideabiz.logger.DirectLogger;
import lk.dialog.ideabiz.logger.model.impl.USSDRequest;
import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.axiata.dialog.oneapi.validation.AxiataException;
import com.axiata.dialog.oneapi.validation.PolicyException;
import com.axiata.dialog.oneapi.validation.RequestError;
import com.axiata.dialog.oneapi.validation.ServiceException;
import com.axiata.dialog.oneapi.validation.impl.ValidateUssdSend;
import com.dialog.mife.ussd.ctrl.RequestManager;
import com.dialog.mife.ussd.ctrl.UrlProvisionEntity;
import com.dialog.mife.ussd.dto.Vxml.Form;
import com.dialog.mife.ussd.dto.Vxml.Form.Block;
import com.dialog.mife.ussd.dto.Vxml.Form.Field;
import com.dialog.mife.ussd.dto.Vxml.Form.Filled;
import com.dialog.mife.ussd.dto.Vxml.Form.Filled.Assign;
import com.dialog.mife.ussd.dto.Vxml.Form.Filled.Goto;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.logging.Level;


import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.net.ssl.HttpsURLConnection;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;


/**
 * @author Charith_02380
 */
@Controller
@RequestMapping("/")
public class UssdAPI {
    private static String routerURL = null;
    private static String routeSkipPrefix = null;
    private static int timeout = 10;
    private static File file = null;

    final static Logger logger = Logger.getLogger(UssdAPI.class);
    private RequestManager manager;
    private Properties settings;
    private Gson gson;
    DirectLogger directLogger = null;

    boolean isvalidate = true;

    /**
     *
     */
    public UssdAPI() {
        try {
            gson = new GsonBuilder().excludeFieldsWithModifiers(Modifier.PROTECTED).serializeNulls().create();

            manager = new RequestManager();
            settings = new Properties();
            settings.loadFromXML(Thread.currentThread().getContextClassLoader().getResourceAsStream("settings.xml"));
            //settings.loadFromXML(UssdAPI.class.getResourceAsStream("settings.xml"));

            String logURL = (settings.getProperty("log_path"));

            if (logURL != null && (logURL.equalsIgnoreCase("null") || logURL.length() < 5))
                logURL = "/home/dialog/mediator.log";
            file = new File(logURL);

            routerURL = settings.getProperty("requestRouter");
            if (routerURL != null && (routerURL.equalsIgnoreCase("null") || routerURL.length() < 10)) {
                routerURL = null;
            }

            routeSkipPrefix = settings.getProperty("requestRouterSkipPrefix");
            if (routeSkipPrefix != null && (routeSkipPrefix.equalsIgnoreCase("null") || routeSkipPrefix.length() < 10)) {
                routeSkipPrefix = routerURL;
            }

            timeout = Integer.parseInt(settings.getProperty("timeout"));


        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        try {
            directLogger = new DirectLogger();
        } catch (Exception e) {
            logger.error("LOGGER INIT:" + e.getMessage(), e);

        }
    }


    @RequestMapping(value = "v1/outbound/{senderAddress}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public ResponseEntity<?> outboundRequest(HttpServletRequest hsr, @PathVariable("senderAddress") String senderAddress, @RequestHeader("X-JWT-Assertion") String jwt, @RequestBody String jsonBody) {
        log("outboundRequest");
        Integer applicationId = 0;

        try {
            applicationId = JWT.getApplicationIdInt(jwt);
        } catch (Exception e) {
            log("Cant read JWT Token");
        }
        try {
            if (isvalidate) {
                new ValidateUssdSend().validate(jsonBody);
            }

            OutboundMessage outboundMsg = gson.fromJson(jsonBody, OutboundMessage.class);
            String msisdn = outboundMsg.getOutboundUSSDMessageRequest().getAddress().replaceAll("tel:+", "").replaceAll("\\+", "").replace("tel:", "");
            Application application = null;

            log("USSD NI:" + applicationId + "|" + outboundMsg.getOutboundUSSDMessageRequest().getShortCode() + "|" + outboundMsg.getOutboundUSSDMessageRequest().getKeyword());

            if (outboundMsg.getOutboundUSSDMessageRequest().getKeyword() == null || outboundMsg.getOutboundUSSDMessageRequest().getKeyword().trim().length() == 0) {
                application = manager.getUniqueApplication(
                        outboundMsg.getOutboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""));
            } else {
                application = manager.getApplication(
                        outboundMsg.getOutboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""),
                        outboundMsg.getOutboundUSSDMessageRequest().getKeyword());

                if (application == null)
                    application = manager.getApplication(outboundMsg.getOutboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""), "*");

            }

            if (outboundMsg.getOutboundUSSDMessageRequest().getSessionID() == null || outboundMsg.getOutboundUSSDMessageRequest().getSessionID().length() < 1) {
                outboundMsg.getOutboundUSSDMessageRequest().setSessionID(hsr.getSession().getId());
            }

            OutboundRequest request = new OutboundRequest();
            request.setApplication(application);
            request.setAMID(applicationId);
            request.setKEYWORD(outboundMsg.getOutboundUSSDMessageRequest().getKeyword());
            request.setPORT(outboundMsg.getOutboundUSSDMessageRequest().getShortCode());
            request.setSession(outboundMsg.getOutboundUSSDMessageRequest().getSessionID());
            request.setOutboundUSSDMessageRequest(outboundMsg.getOutboundUSSDMessageRequest());
            request.setACTION(USSDAction.mtinit.toString());
            Long id = manager.saveRequest(request);

            if (application == null) {
                addLog("1", applicationId, msisdn, outboundMsg.getOutboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""), outboundMsg.getOutboundUSSDMessageRequest().getKeyword(), outboundMsg.getOutboundUSSDMessageRequest().getClientCorrelator(), outboundMsg.getOutboundUSSDMessageRequest().getUssdAction().toString(), outboundMsg.getOutboundUSSDMessageRequest().getSessionID(), "ERROR", "OUTBOUND", outboundMsg.getOutboundUSSDMessageRequest().getOutboundUSSDMessage(), null, "APP_NOT_FOUND", id);
                throw new Exception("Application does not exist");
            }
            if (applicationId < application.getId().intValue()) {
                log("JWT APP IT NOT MATCH WITH APPLICATION ID:" + applicationId + "-JWT:" + application.getAMId() + "-USSD AM ID");
            }


            log("object exists check");
            NIMsisdn msisdnObj = manager.getMSISDN(msisdn);
            log("object is retrived");
            if (msisdnObj != null) {
                log("object exists");
                manager.deleteMSISDN(msisdnObj);
            }
            NIMsisdn msisdnObjSave = new NIMsisdn();
            msisdnObjSave.setMSISDN(msisdn);
            log("saving MSISDN");
            manager.saveMSISDN(msisdnObjSave);
            if (id != null) {
                /**
                 * When an NI USSD is pushed, the gateway calls the registered url for the APP.
                 * Which is ussd/route/{msisdn}
                 * There this key will be used to identify the Application & NI USSD Request
                 * Key format <USSD Action>:<Keyword of the App>:<Request ID>
                 */
                String inputKey =
                        USSDAction.mtinit.toString() + "%3A" +
                                outboundMsg.getOutboundUSSDMessageRequest().getKeyword() + ":" +
                                id;

                String niURL = settings.getProperty("ni_ussd_url_prod");

                if (application.getENVIRONMENT() != null && (application.getENVIRONMENT().equalsIgnoreCase("TEST") || application.getENVIRONMENT().equalsIgnoreCase("TESTBED") || application.getENVIRONMENT().equalsIgnoreCase("SANDBOX"))) {
                    niURL = settings.getProperty("ni_ussd_url_test");
                    log("Using TEST Env : " + niURL);
                }

                Object[] messageparams = {
                        msisdn,
                        outboundMsg.getOutboundUSSDMessageRequest().getShortCode().replaceAll("tel:", "").replaceAll("\\+", ""),
                        inputKey};
                niURL = MessageFormat.format(niURL, messageparams);

                String httpResponse = HTTPRequest(niURL, "GET", null, null, true);

                if (httpResponse != null) {


                    if (httpResponse.contains("SENT")) {
                        outboundMsg.getOutboundUSSDMessageRequest().setDeliveryStatus("SENT");
                        addLog("1", applicationId, msisdn, outboundMsg.getOutboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""), outboundMsg.getOutboundUSSDMessageRequest().getKeyword(), outboundMsg.getOutboundUSSDMessageRequest().getClientCorrelator(), outboundMsg.getOutboundUSSDMessageRequest().getUssdAction().toString(), outboundMsg.getOutboundUSSDMessageRequest().getSessionID(), "SENT", "OUTBOUND", outboundMsg.getOutboundUSSDMessageRequest().getOutboundUSSDMessage(), null, "", id);

                        return new ResponseEntity<Object>(gson.toJson(outboundMsg), HttpStatus.OK);
                    } else {
                        log(httpResponse);
                        outboundMsg.getOutboundUSSDMessageRequest().setDeliveryStatus("NOT SENT");
                        addLog("1", applicationId, msisdn, outboundMsg.getOutboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""), outboundMsg.getOutboundUSSDMessageRequest().getKeyword(), outboundMsg.getOutboundUSSDMessageRequest().getClientCorrelator(), outboundMsg.getOutboundUSSDMessageRequest().getUssdAction().toString(), outboundMsg.getOutboundUSSDMessageRequest().getSessionID(), "NOT_SENT", "OUTBOUND", outboundMsg.getOutboundUSSDMessageRequest().getOutboundUSSDMessage(), null, "NI_ERROR", id);
                        return new ResponseEntity<Object>(gson.toJson(outboundMsg), HttpStatus.OK);


                    }
                } else {
                    outboundMsg.getOutboundUSSDMessageRequest().setDeliveryStatus("NOT SENT");
                    addLog("1", applicationId, msisdn, outboundMsg.getOutboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""), outboundMsg.getOutboundUSSDMessageRequest().getKeyword(), outboundMsg.getOutboundUSSDMessageRequest().getClientCorrelator(), outboundMsg.getOutboundUSSDMessageRequest().getUssdAction().toString(), outboundMsg.getOutboundUSSDMessageRequest().getSessionID(), "NOT_SENT", "OUTBOUND", outboundMsg.getOutboundUSSDMessageRequest().getOutboundUSSDMessage(), null, "ERROR", id);
                    return new ResponseEntity<Object>(gson.toJson(outboundMsg), HttpStatus.OK);
                }


            } else {
                outboundMsg.getOutboundUSSDMessageRequest().setDeliveryStatus("NOT SENT");
                addLog("1", applicationId, msisdn, outboundMsg.getOutboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""), outboundMsg.getOutboundUSSDMessageRequest().getKeyword(), outboundMsg.getOutboundUSSDMessageRequest().getClientCorrelator(), outboundMsg.getOutboundUSSDMessageRequest().getUssdAction().toString(), outboundMsg.getOutboundUSSDMessageRequest().getSessionID(), "NOT_SENT", "OUTBOUND", outboundMsg.getOutboundUSSDMessageRequest().getOutboundUSSDMessage(), null, "EMPTY_ID", id);
                return new ResponseEntity<Object>(gson.toJson(outboundMsg), HttpStatus.OK);

            }

        } catch (AxiataException e) {
            //LOG.error("error charging user: " + senderAddress);
            RequestError requesterror = new RequestError();
            String retstr = "";
            if (e.getErrcode().substring(0, 2).equals("PO")) {
                requesterror.setPolicyException(new PolicyException(e.getErrcode(), e.getErrmsg(), e.getErrvar()[0]));
                //retstr = new Gson().toJson(requesterror);
                retstr = "{\"requestError\":" + new Gson().toJson(requesterror) + "}";
                return new ResponseEntity<Object>(retstr, HttpStatus.FORBIDDEN);

            } else {
                requesterror.setServiceException(new ServiceException(e.getErrcode(), e.getErrmsg(), e.getErrvar()[0]));
                //retstr = new Gson().toJson(requesterror);
                retstr = "{\"requestError\":" + new Gson().toJson(requesterror) + "}";
                return new ResponseEntity<Object>(retstr, HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //return Response.serverError().build();

            RequestError requesterror = new RequestError();
            requesterror.setServiceException(new com.axiata.dialog.oneapi.validation.ServiceException("SVC0001", "Internal Server Error", ""));
            //String jsonreturn = new Gson().toJson(requesterror);
            String jsonreturn = "{\"requestError\":" + new Gson().toJson(requesterror) + "}";
            return new ResponseEntity<Object>(jsonreturn, HttpStatus.FORBIDDEN);

        }
    }

    /**
     * Default landing page for all USSD Requests, the page displayed will depend on the type of the request
     * If a previous mtinit request, the request will be fetched and pushed to the phone.
     * If a moinit, or a mocont, will be forwarded to the apps notify URL
     * In the latter scenario, the app is identified by the keyword, which what the user will dial after the short code.
     * E.g.: #<short code>*<keyword># -> #1721*123#
     *
     * @return
     */
    //TODO
    //SHOULD BE RENAMED TO route/{msisdn}
    @RequestMapping(value = "route/{msisdn}", method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public ResponseEntity<?> ussdRoute(HttpServletRequest hsr, @PathVariable("msisdn") String msisdn, @RequestParam("resp") String resp, @RequestParam("reqID") String reqID) {
//
//        if (!msisdn.startsWith("94"))
//            msisdn = "94" + msisdn;

        log("ussdRoute" + msisdn);

        try {
            String pattern = settings.getProperty("ussd_ni_match");
            String callbackURI = settings.getProperty("ussd_cont");
            if (resp.matches(pattern)) {//Is a call from platform for a previous ni ussd
                log("route USSD MSISDN = " + msisdn);
                NIMsisdn msisdnObj = new NIMsisdn();
                msisdnObj.setMSISDN(msisdn);
                manager.deleteMSISDN(msisdnObj);
                String params[] = resp.split(":");
                OutboundRequest request = manager.getRequest(Long.parseLong(params[2]));


                Object[] messageparams = {
                        msisdn,
                        String.valueOf(request.getId())};
                callbackURI = MessageFormat.format(callbackURI, messageparams);

                request.getOutboundUSSDMessageRequest().setSessionID(hsr.getSession().getId());
                manager.updateRequest(request);

                String xml = createVXMLResponsePage(
                        request.getOutboundUSSDMessageRequest().getOutboundUSSDMessage(),
                        callbackURI);
                log("ResponseNormal=" + resp);

                return new ResponseEntity<Object>(xml, HttpStatus.OK);

            } else {
                log("User Response = " + resp);
                log("Request ID = " + reqID);
                String xml = null;
                OutboundRequest request = manager.getRequest(Long.valueOf(reqID));

                log("Request for : " + gson.toJson(request));
                InboundMessage inboundMessage = createNotifyURLRequest(request, msisdn, resp, getContinueAction(request == null ? null : request.getACTION()));
                String jsonObj = gson.toJson(inboundMessage);
                log("Post to notifyURL jsonbody = " + jsonObj);

                Application application = null;


             /*   try {
                    if (request.getOutboundUSSDMessageRequest().getKeyword() == null || request.getOutboundUSSDMessageRequest().getKeyword().trim().length() == 0) {
                        application = manager.getUniqueApplication(
                                request.getOutboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""));
                    } else {
                        application = manager.getAppliccation(
                                request.getOutboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""),
                                request.getOutboundUSSDMessageRequest().getKeyword());
                    }
                } catch (Exception e) {

                }*/

                application = request.getApplication();
                Integer appId = 0;
                if (application != null) {
                    appId = application.getAMId().intValue();
                }

                addLog("1", appId, msisdn, inboundMessage.getInboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""), inboundMessage.getInboundUSSDMessageRequest().getKeyword(), inboundMessage.getInboundUSSDMessageRequest().getClientCorrelator(), inboundMessage.getInboundUSSDMessageRequest().getUssdAction().toString(), inboundMessage.getInboundUSSDMessageRequest().getSessionID(), "SUCCESS", "INBOUND", inboundMessage.getInboundUSSDMessageRequest().getInboundUSSDMessage(), null, "", Long.valueOf(reqID));

                OutboundMessage outboundmessage = null;

                if (request.getApplication().isBypassKeywordInput() && request.getKEYWORD() != null && request.getKEYWORD().length() > 0) {
                    outboundmessage = new OutboundMessage();
                    outboundmessage.setOutboundUSSDMessageRequest(request.getOutboundUSSDMessageRequest());
                    addLog("1", appId, inboundMessage.getInboundUSSDMessageRequest().getAddress(), request.getPORT(), request.getKEYWORD(), outboundmessage.getOutboundUSSDMessageRequest().getClientCorrelator(), outboundmessage.getOutboundUSSDMessageRequest().getUssdAction().toString(), outboundmessage.getOutboundUSSDMessageRequest().getSessionID(), "SUCCESS", "OUTBOUND", outboundmessage.getOutboundUSSDMessageRequest().getOutboundUSSDMessage(), "SUCCESS", "REPEAT-MOINIT-RESPONSE", Long.valueOf(reqID));
                } else {
                    outboundmessage = postToNotifyURL(jsonObj, request.getOutboundUSSDMessageRequest().getResponseRequest().getNotifyURL(), appId);
                    //logger.info(reqID + ":" + gson.toJson(request));
                    try {
                        if (outboundmessage != null) {
                            //logger.info("=========" + gson.toJson(inboundMessage)+ gson.toJson(outboundmessage) + gson.toJson(request));
                            addLog("1", appId, inboundMessage.getInboundUSSDMessageRequest().getAddress(), request.getPORT(), request.getKEYWORD(), outboundmessage.getOutboundUSSDMessageRequest().getClientCorrelator(), outboundmessage.getOutboundUSSDMessageRequest().getUssdAction().toString(), outboundmessage.getOutboundUSSDMessageRequest().getSessionID(), "SUCCESS", "OUTBOUND", outboundmessage.getOutboundUSSDMessageRequest().getOutboundUSSDMessage(), "SUCCESS", "", Long.valueOf(reqID));
                        } else {
                            addLog("1", appId, inboundMessage.getInboundUSSDMessageRequest().getAddress(), request.getPORT(), request.getKEYWORD(), null, null, null, "ERROR", "OUTBOUND", null, "FAILED", request.getOutboundUSSDMessageRequest().getResponseRequest().getNotifyURL(), Long.valueOf(reqID));
                        }
                    } catch (Exception e) {
                        logger.error("Log error ", e);
                    }
                }


                if (outboundmessage.getOutboundUSSDMessageRequest().getUssdAction() == USSDAction.mtfin) {
                    xml = createVXMLFinalPage(outboundmessage.getOutboundUSSDMessageRequest().getOutboundUSSDMessage());
                } else {
                    Object[] messageparams = {
                            msisdn,
                            String.valueOf(request.getId())};
                    callbackURI = MessageFormat.format(callbackURI, messageparams);
                    xml = createVXMLResponsePage(
                            outboundmessage.getOutboundUSSDMessageRequest().getOutboundUSSDMessage(),
                            callbackURI);
                    log("Response=" + resp);
                }
                request.setOutboundUSSDMessageRequest(outboundmessage.getOutboundUSSDMessageRequest());
                manager.saveRequest(request);
                return new ResponseEntity<Object>(xml, HttpStatus.OK);

            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }


    @RequestMapping(value = "v1/inbound/subscriptions", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> inboundSub(HttpServletRequest hsr, @RequestHeader("X-JWT-Assertion") String jwt, @RequestBody String jsonBody) {


        log("Inbbound Subscription:" + jsonBody);
        Integer applicationId = null;

        try {
            applicationId = JWT.getApplicationIdInt(jwt);
        } catch (Exception e) {
            log("Cant read JWT Token");
        }


        Application application = null;


        try {
            USSDSubscriptionWrap ussdSubscriptionWrap = gson.fromJson(jsonBody, USSDSubscriptionWrap.class);
            String shortcode = ussdSubscriptionWrap.getSubscription().getShortCode();
            shortcode = shortcode.replace("*", "");
            shortcode = shortcode.replace("#", "");
            shortcode = shortcode.replace("tel:+", "");
            shortcode = shortcode.replace("tel:", "");

            ussdSubscriptionWrap.getSubscription().setShortCode(ussdSubscriptionWrap.getSubscription().getShortCode().replace("tel:+", ""));
            ussdSubscriptionWrap.getSubscription().setShortCode(ussdSubscriptionWrap.getSubscription().getShortCode().replace("tel:", ""));
            ussdSubscriptionWrap.getSubscription().setShortCode(ussdSubscriptionWrap.getSubscription().getShortCode().replace("+", ""));

            if (ussdSubscriptionWrap.getSubscription().getKeyword() == null || ussdSubscriptionWrap.getSubscription().getKeyword().equalsIgnoreCase("")) {
                application = manager.getUniqueApplication(shortcode);

            } else {
                application = manager.getApplication(shortcode, ussdSubscriptionWrap.getSubscription().getKeyword());
            }

            if (application == null) {
                Application application1 = new Application();
                application1.setAMId(new Long(applicationId));
                application1.setKeyword(ussdSubscriptionWrap.getSubscription().getKeyword());
                application1.setShortCode(shortcode);
                application1.setBypassKeywordInput(false);
//               application1.setServiceProvider();
                application1.setUnique(false);

                application1 = manager.saveSubscription(application1);


                UrlProvisionEntity urlProvisionEntity = new UrlProvisionEntity();
                urlProvisionEntity.setShortCode(shortcode);
                urlProvisionEntity.setKeyWord(ussdSubscriptionWrap.getSubscription().getKeyword());
                urlProvisionEntity.setApplication(application1);
                urlProvisionEntity.setNotifyUrl(ussdSubscriptionWrap.getSubscription().getCallbackReference().getNotifyURL());
                urlProvisionEntity = manager.saveURL(urlProvisionEntity);

                String URL = settings.getProperty("adaptorURL");

                USSDSubscriptionResponse ussdSubscriptionResponse = new USSDSubscriptionResponse();
                ussdSubscriptionResponse.setShortCode("tel:" + shortcode);
                ussdSubscriptionResponse.setKeyword(ussdSubscriptionWrap.getSubscription().getKeyword());
                ussdSubscriptionResponse.setResourceURL(URL + "inbound/subscriptions/sub" + urlProvisionEntity.getId());

                ussdSubscriptionResponse.setCallbackReference(ussdSubscriptionWrap.getSubscription().getCallbackReference());
                ussdSubscriptionResponse.setClientCorrelator(ussdSubscriptionWrap.getSubscription().getClientCorrelator());

                USSDSubscriptionResponseWrap ussdSubscriptionResponseWrap = new USSDSubscriptionResponseWrap();
                ussdSubscriptionResponseWrap.setSubscription(ussdSubscriptionResponse);
                return new ResponseEntity<Object>(gson.toJson(ussdSubscriptionResponseWrap), HttpStatus.CREATED);

            } else {
                logger.error("Duplicate subscription");

                RequestError requesterror = new RequestError();
                String retstr = "";

                requesterror.setPolicyException(new PolicyException("SVC0008", "Overlapped criteria %1", ussdSubscriptionWrap.getSubscription().getKeyword()));
                retstr = new Gson().toJson(requesterror);
                return new ResponseEntity<Object>(retstr, HttpStatus.FORBIDDEN);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    @RequestMapping(value = "v1/inbound/subscriptions/sub{id}", method = RequestMethod.DELETE, produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> inboundSub(HttpServletRequest hsr, @RequestHeader("X-JWT-Assertion") String jwt, @PathVariable("id") Long id) {


        Integer applicationId = null;

        try {
            applicationId = JWT.getApplicationIdInt(jwt);
        } catch (Exception e) {
            log("Cant read JWT Token");
        }
        log("Delete Subscription:" + id + " " + applicationId);

        try {
            UrlProvisionEntity urlProvisionEntity = manager.getURLById(id);

            logger.info("App " + id + " " + gson.toJson(urlProvisionEntity));
            if (urlProvisionEntity.getApplication().getAMId().intValue() == applicationId.intValue()) {

                // manager.deleteSubscription(urlProvisionEntity.getApplication());
                String port = urlProvisionEntity.getShortCode();
                String keyword = urlProvisionEntity.getKeyWord();


                Application app = urlProvisionEntity.getApplication();
                manager.deleteURL(urlProvisionEntity);

                manager.deleteSubscription(app);


                return new ResponseEntity<Object>("", HttpStatus.OK);

            } else {
                logger.error("Application not match");
                return new ResponseEntity<Object>(gson.toJson(new ServiceException("SVC0001", "Internal Server Error", "")), HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

        }

    }

    //TODO
    //SHOULD BE RENAMED TO default/{msisdn}
    @RequestMapping(value = "mo-init/{port}/{msisdn}", method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public ResponseEntity<?> ussdDefault(HttpServletRequest hsr, @PathVariable("msisdn") String msisdn, @PathVariable("port") String port, @RequestParam("Keyword") String keyword) {
        log("ussdDefault");
        log(port + ":" + msisdn);
        Long id = null;
        Application application = null;
        Integer AMID = 0;
        String session = null;

        try {
            String callbackURI = settings.getProperty("ussd_callback");
            if (msisdn != null && !msisdn.startsWith("94"))
                msisdn = "94" + msisdn;
            NIMsisdn msisdnObj = manager.getMSISDN(msisdn);
            OutboundRequest request = null;
            String vxml = null;
            USSDAction action = USSDAction.moinit;

            if (msisdnObj != null && false) {
                Object[] messageparams = {msisdn, String.valueOf(System.currentTimeMillis())};
                callbackURI = MessageFormat.format(callbackURI, messageparams);
                log("MSISDN is not null");
                manager.deleteMSISDN(msisdnObj);
                vxml = createVXMLResponsePage("Error in Application. \nPlease try again", callbackURI);
            } else {
                if (keyword.contains("mtinit")) {
                    action = USSDAction.mtcont;

                    try {
                        keyword = keyword.split(":")[1];
                        Long oid = Long.parseLong(keyword.split(":")[2]);
                        request = manager.getRequest(oid);
                    } catch (Exception e) {

                    }
                }
                log("MSISDN is null - Mobile originated |" + port + "|" + keyword + "|" + msisdn + "|" + action.toString());

                //log("Short code from settings:" + settings.getProperty("shortcode"));

                //String provisionedNotifyUrl = manager.getNotifyUrlByShortCode(settings.getProperty("shortcode"));
                String provisionedNotifyUrl = null;

                if (keyword != null && keyword.length() > 0 && !keyword.isEmpty()) {
                    try {
                        provisionedNotifyUrl = manager.getNotifyUrlByKeyword(port, keyword);
                        application = manager.getApplication(port, keyword);

                    } catch (Exception ex) {
                        log("No  port:keyword individual URL : try to sending to wildcard");
                    }

                    if (provisionedNotifyUrl == null) {
                        provisionedNotifyUrl = manager.getNotifyUrlByKeyword(port, "*");
                        if (application == null)
                            application
                                    = manager.getApplication(port, "*");

                    }
                } else {
                    provisionedNotifyUrl = manager.getNotifyUrlByShortCode(port);
                    application = manager.getUniqueApplication(port);
                }

                log("provisioned URL = " + provisionedNotifyUrl);

                //String notifyUrl = "http://10.62.96.187:9764/mavenproject1-1.0-SNAPSHOT/webresources/endpoint/ussd/init" +"?msisdn="+ msisdn;

//                String notifyUrl = provisionedNotifyUrl + "?msisdn=" + msisdn;
//
//                log("notifyURL = " + notifyUrl);

//                OutboundMessage outboundMsg = notifyApp(notifyUrl, msisdn);

                try {
                    session = hsr.getSession().getId();
                } catch (Exception e) {
                }

                if (application != null && application.getAMId() != null)
                    AMID = application.getAMId().intValue();

                OutboundMessage outboundMsg = initNotify(msisdn, port, keyword, provisionedNotifyUrl, session, action, AMID);


         /*       Application application = null;
                if (outboundMsg.getOutboundUSSDMessageRequest().getKeyword() == null || outboundMsg.getOutboundUSSDMessageRequest().getKeyword().trim().length() == 0) {
                    application = manager.getUniqueApplication(
                            outboundMsg.getOutboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""));
                } else {
                    application = manager.getApplication(
                            outboundMsg.getOutboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""),
                            outboundMsg.getOutboundUSSDMessageRequest().getKeyword());
                }*/


                if (outboundMsg == null) {
                    addLog("1", AMID, msisdn, port, keyword, "", action.toString(), session, "SUCCESS", "INBOUND", keyword, "SUCCESS", "", id);
                    addLog("1", AMID, msisdn, port, keyword, "", action.toString(), session, "ERROR", "OUTBOUND", "", "HTTP", provisionedNotifyUrl, id);
                }

                if (request == null) {
                    request = new OutboundRequest();
                    request.setApplication(application);
                    request.setOutboundUSSDMessageRequest(outboundMsg.getOutboundUSSDMessageRequest());
                    request.setAMID(AMID);
                    request.setPORT(port);
                    request.setKEYWORD(keyword);
                    request.setSession(session);
                    request.setACTION(USSDAction.moinit.toString());
                }


                id = manager.saveRequest(request);

                if (outboundMsg != null) {
                    addLog("1", AMID, msisdn, port, keyword, "", "mo-init", session, "SUCCESS", "INBOUND", keyword, "SUCCESS", "", id);
                    addLog("1", AMID, msisdn, port, keyword, outboundMsg.getOutboundUSSDMessageRequest().getClientCorrelator(), outboundMsg.getOutboundUSSDMessageRequest().getUssdAction().toString(), outboundMsg.getOutboundUSSDMessageRequest().getSessionID(), "SUCCESS", "OUTBOUND", outboundMsg.getOutboundUSSDMessageRequest().getOutboundUSSDMessage(), "SUCCESS", "", id);
                }

                String reqID = id.toString();

                Object[] messageparams = {msisdn, reqID};
                callbackURI = MessageFormat.format(callbackURI, messageparams);
                log("Callback URL: " + callbackURI);
                String message = "";
                message = request.getOutboundUSSDMessageRequest().getOutboundUSSDMessage();

                if (outboundMsg.getOutboundUSSDMessageRequest().getUssdAction() == USSDAction.mtfin) {
                    vxml = createVXMLFinalPage(outboundMsg.getOutboundUSSDMessageRequest().getOutboundUSSDMessage());
                } else {
                    vxml = createVXMLResponsePage(message, callbackURI);
                }

            }

            return new ResponseEntity<Object>(vxml, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            addLog("1", AMID, msisdn, port, keyword, session, "mo-init", "", "ERROR", "OUTBOUND", "", "EXCEPTION", "", id);

            try {
                String vxml = createVXMLFinalPage("Error in application.");
                return new ResponseEntity<Object>(vxml, HttpStatus.OK);
            } catch (Exception ex) {
                return new ResponseEntity<Object>("", HttpStatus.INTERNAL_SERVER_ERROR);
            }

        }
    }


    @RequestMapping(value = "mt-cont/{msisdn}", method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public ResponseEntity<?> mtContUSSD(HttpServletRequest hsr, @PathVariable("msisdn") String msisdn, @RequestParam("resp") String resp, @RequestParam("reqID") String reqID) {
        log("mtContUSSD:" + msisdn + ":" + resp + "|" + reqID);
        Application app = null;
        Integer AMID = 0;


        try {
            OutboundRequest request = manager.getRequest(Long.valueOf(reqID));
            if (msisdn != null && msisdn.startsWith("7"))
                msisdn = "94" + msisdn;


            InboundUSSDMessageRequest inboundUSSDMessageRequest = new InboundUSSDMessageRequest();
            inboundUSSDMessageRequest.setAddress("tel:+" + msisdn);
            inboundUSSDMessageRequest.setSessionID(request.getSession());
            inboundUSSDMessageRequest.setClientCorrelator(request.getOutboundUSSDMessageRequest().getClientCorrelator());
            inboundUSSDMessageRequest.setInboundUSSDMessage(resp);
            inboundUSSDMessageRequest.setKeyword(request.getKEYWORD());
            inboundUSSDMessageRequest.setShortCode(request.getPORT());
            inboundUSSDMessageRequest.setResponseRequest(request.getOutboundUSSDMessageRequest().getResponseRequest());
            inboundUSSDMessageRequest.setUssdAction(getContinueAction(request.getACTION()));
            InboundMessage inboundMessage = new InboundMessage();
            inboundMessage.setInboundUSSDMessageRequest(inboundUSSDMessageRequest);


          /*  if (request.getApplication() == null || inboundMessage.getInboundUSSDMessageRequest().getKeyword().trim().length() == 0) {
                app = manager.getUniqueApplication(
                       request.getPORT());
            } else {
                app = manager.getApplication(
                        inboundMessage.getInboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""),
                        inboundMessage.getInboundUSSDMessageRequest().getKeyword());

                if (app == null) {
                    app = manager.getApplication(
                            inboundMessage.getInboundUSSDMessageRequest().getShortCode().replace("tel:", "").replaceAll("\\+", ""), "*");
                }
            }*/
            app = request.getApplication();

            if (app != null && app.getAMId() != null)
                AMID = app.getAMId().intValue();

            if (AMID == 0)
                AMID = request.getAMID();

            addLog("1", AMID, msisdn, request.getPORT(), request.getKEYWORD(), inboundMessage.getInboundUSSDMessageRequest().getClientCorrelator(), inboundMessage.getInboundUSSDMessageRequest().getUssdAction().toString(), request.getSession(), "SUCCESS", "INBOUND", inboundMessage.getInboundUSSDMessageRequest().getInboundUSSDMessage(), "SUCCESS", "", Long.valueOf(reqID));


            if (app == null) {
                logger.error("Application not found ");
            }


            HttpPost post = new HttpPost(getRouteURL(request.getOutboundUSSDMessageRequest().getResponseRequest().getNotifyURL(), AMID));
            //post.setConfig(requestConfig);

            String jsonObj = gson.toJson(inboundMessage);

            CloseableHttpClient httpclient = null;
            CloseableHttpResponse response = null;
            try {
                httpclient = HttpClients.createDefault();/*HttpClients.custom()
                        .setConnectionManager(connectionManager)
		                .build();*/

                StringEntity strEntity = new StringEntity(jsonObj.toString(), "UTF-8");
                strEntity.setContentType("application/json");
                post.setEntity(strEntity);

                response = httpclient.execute(post);
                HttpEntity entity = response.getEntity();

                String xml = createVXMLFinalPage("Thank You");

                if (entity != null) {
                    InputStream instream = entity.getContent();
                    try {
                        StringWriter writer = new StringWriter();
                        IOUtils.copy(new InputStreamReader(instream), writer, 1024);
                        String body = writer.toString();
                        OutboundMessage outboundMsg = gson.fromJson(body, OutboundMessage.class);

                        addLog("1", AMID, inboundMessage.getInboundUSSDMessageRequest().getAddress(), inboundMessage.getInboundUSSDMessageRequest().getShortCode(), inboundMessage.getInboundUSSDMessageRequest().getKeyword(), inboundMessage.getInboundUSSDMessageRequest().getClientCorrelator(), outboundMsg.getOutboundUSSDMessageRequest().getUssdAction().toString(), outboundMsg.getOutboundUSSDMessageRequest().getSessionID(), "SUCCESS", "OUTBOUND", outboundMsg.getOutboundUSSDMessageRequest().getOutboundUSSDMessage(), "SUCCESS", "", Long.valueOf(reqID));
                        if (outboundMsg.getOutboundUSSDMessageRequest().getUssdAction() == USSDAction.mtcont || outboundMsg.getOutboundUSSDMessageRequest().getUssdAction() == USSDAction.mocont) {
//                            OutboundRequest outBoundRequest = new OutboundRequest();
//                            outBoundRequest.setApplication(app);
//                            outBoundRequest.setSession(request.getSession());
//                            outBoundRequest.setAMID(request.getAMID());
//                            outBoundRequest.setPORT(request.getPORT());
//                            outBoundRequest.setKEYWORD(request.getKEYWORD());
//                            outBoundRequest.setOutboundUSSDMessageRequest(outboundMsg.getOutboundUSSDMessageRequest());
//                            Long id = manager.saveRequest(outBoundRequest);

                            //use same ID for one session
                            request.setOutboundUSSDMessageRequest(outboundMsg.getOutboundUSSDMessageRequest());
                            Long id = manager.saveRequest(request);


                            if (id != null) {
                                String callbackURI = settings.getProperty("ussd_cont");
                                Object[] messageparams = {
                                        msisdn,
//                                        String.valueOf(outBoundRequest.getId())};
                                        String.valueOf(request.getId())};
                                callbackURI = MessageFormat.format(callbackURI, messageparams);
                                xml = createVXMLResponsePage(outboundMsg.getOutboundUSSDMessageRequest().getOutboundUSSDMessage(), callbackURI);
                            }
                        }
                        if (outboundMsg.getOutboundUSSDMessageRequest().getUssdAction() == USSDAction.mtfin) {
                            xml = createVXMLFinalPage(outboundMsg.getOutboundUSSDMessageRequest().getOutboundUSSDMessage());
                        }
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                        addLog("1", AMID, inboundMessage.getInboundUSSDMessageRequest().getAddress(), inboundMessage.getInboundUSSDMessageRequest().getShortCode(), inboundMessage.getInboundUSSDMessageRequest().getKeyword(), inboundMessage.getInboundUSSDMessageRequest().getClientCorrelator(), inboundMessage.getInboundUSSDMessageRequest().getUssdAction().toString(), inboundMessage.getInboundUSSDMessageRequest().getSessionID(), "ERROR", "OUTBOUND", "", "EXCEPTION", request.getOutboundUSSDMessageRequest().getResponseRequest().getNotifyURL(), Long.valueOf(reqID));

                    }
                }
                return new ResponseEntity<Object>(xml, HttpStatus.OK);

            } catch (Exception e) {
                addLog("1", AMID, inboundMessage.getInboundUSSDMessageRequest().getAddress(), inboundMessage.getInboundUSSDMessageRequest().getShortCode(), inboundMessage.getInboundUSSDMessageRequest().getKeyword(), inboundMessage.getInboundUSSDMessageRequest().getClientCorrelator(), inboundMessage.getInboundUSSDMessageRequest().getUssdAction().toString(), inboundMessage.getInboundUSSDMessageRequest().getSessionID(), "ERROR", "OUTBOUND", "", "EXCEPTION", request.getOutboundUSSDMessageRequest().getResponseRequest().getNotifyURL(), Long.valueOf(reqID));
                logger.error(e.getMessage(), e);
                return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
            } finally {
                response.close();
                httpclient.close();
            }

        } catch (Exception e) {
            logger.error(e.getMessage(), e);

            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    private OutboundMessage initNotify(String msisdn, String port, String keyword, String url, String sessionId, USSDAction action, Integer amid) {


        InboundUSSDMessageRequest inboundUSSDMessageRequest = new InboundUSSDMessageRequest();
        inboundUSSDMessageRequest.setAddress("tel:+" + msisdn);
        inboundUSSDMessageRequest.setSessionID(sessionId);
        inboundUSSDMessageRequest.setClientCorrelator(msisdn + "-" + sessionId);
        inboundUSSDMessageRequest.setInboundUSSDMessage(null);
        inboundUSSDMessageRequest.setKeyword(keyword);
        inboundUSSDMessageRequest.setShortCode(port);
        inboundUSSDMessageRequest.setResponseRequest(new ResponseRequest());
        inboundUSSDMessageRequest.setUssdAction(action);
        inboundUSSDMessageRequest.getResponseRequest().setNotifyURL(url);
        InboundMessage inboundMessage = new InboundMessage();
        inboundMessage.setInboundUSSDMessageRequest(inboundUSSDMessageRequest);

        String jsonObj = gson.toJson(inboundMessage);

        return postToNotifyURL(jsonObj, url, amid);

    }

    private InboundMessage createNotifyURLRequest(OutboundRequest request, String msisdn, String resp, USSDAction action) {

        if (msisdn != null && !msisdn.startsWith("94"))
            msisdn = "94" + msisdn;

        InboundUSSDMessageRequest inboundUSSDMessageRequest = new InboundUSSDMessageRequest();
        inboundUSSDMessageRequest.setAddress("tel:+" + msisdn);
        inboundUSSDMessageRequest.setSessionID(request.getSession());
        inboundUSSDMessageRequest.setClientCorrelator(msisdn + "-" + request.getSession());
        inboundUSSDMessageRequest.setInboundUSSDMessage(resp);
        inboundUSSDMessageRequest.setKeyword(request.getKEYWORD());
        inboundUSSDMessageRequest.setShortCode(request.getPORT());
        inboundUSSDMessageRequest.setResponseRequest(request.getOutboundUSSDMessageRequest().getResponseRequest());
        inboundUSSDMessageRequest.setUssdAction(action);
        InboundMessage inboundMessage = new InboundMessage();
        inboundMessage.setInboundUSSDMessageRequest(inboundUSSDMessageRequest);


        return inboundMessage;
    }

    private OutboundMessage postToNotifyURL(String jsonObj, String notifyURL, Integer amID) {
        try {
            HttpPost post = new HttpPost(getRouteURL(notifyURL, amID));

            CloseableHttpClient httpclient = null;
            CloseableHttpResponse response = null;

            httpclient = HttpClients.createDefault();/*HttpClients.custom()
            .setConnectionManager(connectionManager)
            .build();*/

            StringEntity strEntity = new StringEntity(jsonObj.toString(), "UTF-8");
            strEntity.setContentType("application/json");
            post.setEntity(strEntity);

            response = httpclient.execute(post);
            HttpEntity entity = response.getEntity();

            InputStream instream = entity.getContent();

            StringWriter writer = new StringWriter();
            IOUtils.copy(new InputStreamReader(instream), writer, 1024);
            String body = writer.toString();
            log("Return from notifyURL = " + body);

            OutboundMessage outboundMsg = gson.fromJson(body, OutboundMessage.class);
            return outboundMsg;

        } catch (IOException ex) {
            log(ex.getMessage());
            return null;
        }

    }

    private OutboundMessage notifyApp(String notifyURL, String msisdn) throws IOException {

        CloseableHttpClient httpclient = null;
        CloseableHttpResponse response = null;

        httpclient = HttpClients.createDefault();
        String body = null;
        //notify URL should be retrived from APP
        // String appNotifyURL = "" + msisdn;

        HttpGet get = new HttpGet(notifyURL);

        try {

            response = httpclient.execute(get);

            HttpEntity entity = response.getEntity();


            if (entity != null) {
                InputStream instream = entity.getContent();

                StringWriter writer = new StringWriter();

                IOUtils.copy(new InputStreamReader(instream), writer, 1024);
                body = writer.toString();
            }
        } catch (IOException ex) {
            java.util.logging.Logger.getLogger(UssdAPI.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                response.close();
                httpclient.close();
            } catch (IOException ex) {
                java.util.logging.Logger.getLogger(UssdAPI.class.getName()).log(Level.SEVERE, null, ex);
                return null;
            }

        }

        log("Json Body from APP = " + body);
        OutboundMessage outboundMsg = gson.fromJson(body, OutboundMessage.class);


        return outboundMsg;
    }

    private static void log(String text) {

//      /*  PrintWriter out = null;
//        try {
//            out = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
//            out.println(text);
//        } catch (IOException e) {
//            System.err.println(e);
//        } finally {
//            if (out != null) {
//                out.close();
//            }
//        }*/
        logger.info(text);
    }

    /**
     * @param prompt
     * @param callbackURI
     * @return
     */
    private String createVXMLResponsePage(String prompt, String callbackURI) throws JAXBException, PropertyException {
        return createVXMLPage(prompt, callbackURI, "vResponse", "oc_message", "#responseMsg");
    }

    /**
     * @param prompt
     * @return
     * @throws JAXBException
     * @throws PropertyException
     */
    private String createVXMLFinalPage(String prompt) throws JAXBException, PropertyException {
        /*VXMLFactory factory = new VXMLFactory();
        Vxml vxml = factory.createVxml();
		
		//<field name="oc_final">
		//	<prompt>END</prompt>
		//</field>
		Field field = factory.createVxmlFormField();
		field.setName("oc_final");
		field.setPrompt(prompt);
		
		//<filled>
		//	<assign name="" expr="oc_final"/>
		//	<goto next=""/>
		//</filled>
		Assign assign = factory.createVxmlFormFilledAssign();
		assign.setName("");
		assign.setExpr("oc_final");
		Goto gotoElem = factory.createVxmlFormFilledGoto();
		gotoElem.setNext("");			
		Filled filled = factory.createVxmlFormFilled();
		filled.setAssign(assign);
		filled.setGoto(gotoElem);
		
		//<form id="final" name="Final Form">
		//	<field/>
		//	<filled/>
		//</form>
		Form form1 = factory.createVxmlForm();
		form1.setId("final");
		form1.setName("Final Form");
		form1.setField(field);
		form1.setFilled(filled);

		List<Form> formList = vxml.getForm();
		formList.add(0, form1);

		JAXBContext context = JAXBContext.newInstance(Vxml.class);

		Marshaller m = context.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		m.setProperty("com.sun.xml.bind.xmlDeclaration", Boolean.FALSE);

		StringWriter writer = new StringWriter();
		m.marshal(vxml, writer);

		String xml = writer.toString();*/
        /**
         * This is a dirty hack
         * FIX this properly
         */
        String xml =
                "<vxml>" +
                        "	<form id=\"Menu Item1\" name=\"Menu Item1 ID\">" +
                        "		<field name=\"var1\">" +
                        "			<prompt><![CDATA[" + prompt + "]]></prompt>" +
                        "		</field>" +
                        "		<property name=\"oc_bIsFinal\" value=\"1\"/>" +
                        "	</form>" +
                        "</vxml>";

        return xml;
    }

    /**
     * @param prompt
     * @param callbackURI
     * @return
     * @throws JAXBException
     * @throws PropertyException
     */
    private String createVXMLPage(String prompt, String callbackURI, String filledName, String filledExpr, String gotoURL) throws JAXBException, PropertyException {
        VXMLFactory factory = new VXMLFactory();
        Vxml vxml = factory.createVxml();

        //<field name="oc_message">
        //	<prompt>Hello 94777335365!\n1. Continue\n2. Exit</prompt>
        //</field>
        Field field = factory.createVxmlFormField();
        field.setName("oc_message");
        field.setPrompt(prompt);

        //<filled>
        //	<assign name="vResponse" expr="oc_message"/>
        //	<goto next="#responseMsg"/>
        //</filled>
        Assign assign = factory.createVxmlFormFilledAssign();
        assign.setName(filledName);// "vResponse");
        assign.setExpr(filledExpr);// "oc_message");
        Goto gotoElem = factory.createVxmlFormFilledGoto();
        gotoElem.setNext(gotoURL);// "#responseMsg");
        Filled filled = factory.createVxmlFormFilled();
        filled.setAssign(assign);
        filled.setGoto(gotoElem);

        //<form id="MenuID Here" name="Menu Name Here">
        //	<field/>
        //	<filled/>
        //</form>
        Form form1 = factory.createVxmlForm();
        form1.setId("MainMenu");
        form1.setName("Main Menu");
        form1.setField(field);
        form1.setFilled(filled);

        //<block name="oc_ActionUrl">
        //	<goto next="http://172.22.163.88:8080/ussd/route/{0}?resp=%vResponse%&reqID=%vReqID%" />
        //</block>
        com.dialog.mife.ussd.dto.Vxml.Form.Block.Goto blockGoto = factory.createVxmlFormBlockGoto();
        blockGoto.setNext(callbackURI);
        Block block = factory.createVxmlFormBlock();
        block.setName("oc_ActionUrl");
        block.setGoto(blockGoto);

        //<form id="responseMsg" name="responseMsg">
        //<block/>
        //</form>
        Form form2 = factory.createVxmlForm();
        form2.setId("responseMsg");
        form2.setName("responseMsg");
        form2.setBlock(block);

        List<Form> formList = vxml.getForm();
        formList.add(0, form1);
        formList.add(1, form2);

        JAXBContext context = JAXBContext.newInstance(Vxml.class);

        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.setProperty("com.sun.xml.bind.xmlDeclaration", false);

        StringWriter writer = new StringWriter();
        m.marshal(vxml, writer);

        String xml = writer.toString();
        return xml;
    }


    @RequestMapping(value = "routeNotifyUrl/{routeNotifyUrl}", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public ResponseEntity<?> provisionNotifyUrl(HttpServletRequest servletRequest, @PathVariable("routeNotifyUrl") String routeNotifyUrl) {
        try {
            UrlProvisionEntity urlProvisionEntity = gson.fromJson(routeNotifyUrl, UrlProvisionEntity.class);
            String provisionNotifyUrl = manager.provisionNotifyUrl(urlProvisionEntity.getApplication().getId());
            String jsonObj = gson.toJson(provisionNotifyUrl);
            return new ResponseEntity<Object>(jsonObj, HttpStatus.OK);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return new ResponseEntity<Object>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }

    public void addLog(String version, Integer appid, String address, String shortCode, String keyword, String clientCorrelator, String ussdAction, String sessionId, String status, String direction, String message, String applicationStatus, String description, Long requestId) {
        try {
            logger.info(version + ":" + appid + ":" + address + ":" + shortCode + ":" + keyword + ":" + clientCorrelator + ":" + ussdAction + ":" + sessionId + ":" + status + ":" + direction + ":" + message + ":" + applicationStatus + ":" + description + ":" + requestId);
            directLogger.addLog(new USSDRequest(version, "DIALOG", appid, address, shortCode, keyword, clientCorrelator, ussdAction, sessionId, status, direction, message, applicationStatus, description, requestId));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }

    public String getRouteURL(String inURL, Integer amID) {
        //return inURL;
        try {
            if (inURL == null)
                return null;

            if (routerURL == null)
                return inURL;

            if (routeSkipPrefix != null && inURL.toLowerCase().startsWith(routeSkipPrefix.toLowerCase())) {
                log("Skip Route -ALREADY HAVE IN URL : " + inURL);
                return inURL;
            }

            if (amID == null)
                amID = 0;
            inURL = routerURL + URLEncoder.encode(inURL) + "&API=USSD&APIV=2&AMID=" + amID + "&timeout=" + timeout;
            log("Updated URL: " + inURL);
            return inURL;
        } catch (Exception e) {
            log("Error Update URL: " + inURL);
            return inURL;
        }

    }

    public String HTTPRequest(String url, String method, String body, String contentType, boolean direct) {
        log("Sending 'GET' request to URL : " + url);


        HttpURLConnection con = null;
        try {
            URL obj = new URL(url);
            if (url.startsWith("https")) {
                con = (HttpsURLConnection) obj.openConnection();
            } else {
                con = (HttpURLConnection) obj.openConnection();
            }
            con.setRequestMethod("POST");
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Accept-Charset", "iso-8859-5, unicode-1-1;q=0.8");


            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.flush();
            wr.close();
            int resCode = 0;

            resCode = con.getResponseCode();

            log("Response Code : " + resCode + "|" + con.getContentEncoding() + "|" + url);

            BufferedReader in = null;

            con.getContentEncoding();

            if (resCode >= 400) {
                in = new BufferedReader(
                        new InputStreamReader(con.getErrorStream(), "UTF-8"));
            } else {
                in = new BufferedReader(
                        new InputStreamReader(con.getInputStream(), "UTF-8"));
            }

            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            log("Response : " + "|" + url + "|" + response.toString());

            return response.toString();
        } catch (Exception ex) {
            log("Connection Error : " + "|" + url + "|" + ex.getMessage());

        }
        return null;
    }

    USSDAction getContinueAction(String action) {

        try {
            USSDAction u = USSDAction.valueOf(action);
            if (u == USSDAction.mtinit)
                return USSDAction.mtcont;

            return USSDAction.mocont;
        } catch (Exception e) {
            return USSDAction.mocont;
        }
    }
}
