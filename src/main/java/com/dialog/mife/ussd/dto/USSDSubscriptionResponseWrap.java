package com.dialog.mife.ussd.dto;

/**
 * Created by Malinda_07654 on 4/12/2016.
 */
public class USSDSubscriptionResponseWrap {
    USSDSubscriptionResponse subscription;

    public USSDSubscriptionResponse getSubscription() {
        return subscription;
    }

    public void setSubscription(USSDSubscriptionResponse subscription) {
        this.subscription = subscription;
    }
}
