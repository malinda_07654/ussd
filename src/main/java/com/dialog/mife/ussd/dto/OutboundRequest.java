package com.dialog.mife.ussd.dto;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * 
 * @author Charith_02380
 *
 */
@Entity
@Table(name="OUTBOUND_REQUEST")
public class OutboundRequest {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="REQ_ID", unique=true, insertable=false, updatable=false)
    private Long id; 
	
	@ManyToOne( cascade = {CascadeType.PERSIST, CascadeType.MERGE} )
    @JoinColumn(name="APP_ID")
	private Application application;
	
	@OneToOne( cascade = {CascadeType.ALL} )
	@JoinColumn(name="MESSAGE")
	private OutboundUSSDMessageRequest outboundUSSDMessageRequest;

	@Column(name = "SESSION")
	private String session;

	@Column(name = "AM_ID")
	private Integer AMID;

	@Column(name = "PORT")
	private String PORT;

	@Column(name = "KEYWORD")
	private String KEYWORD;

	@Column(name = "ACTION")
	private String ACTION;


	public OutboundRequest() {
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @return the application
	 */
	public Application getApplication() {
		return application;
	}

	/**
	 * @return the outboundUSSDMessageRequest
	 */
	public OutboundUSSDMessageRequest getOutboundUSSDMessageRequest() {
		return outboundUSSDMessageRequest;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param application the application to set
	 */
	public void setApplication(Application application) {
		this.application = application;
	}


	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public Integer getAMID() {
		return AMID;
	}

	public void setAMID(Integer AMID) {
		this.AMID = AMID;
	}

	public String getPORT() {
		return PORT;
	}

	public void setPORT(String PORT) {
		this.PORT = PORT;
	}

	public String getKEYWORD() {
		return KEYWORD;
	}

	public void setKEYWORD(String KEYWORD) {
		this.KEYWORD = KEYWORD;
	}

	public String getACTION() {
		return ACTION;
	}

	public void setACTION(String ACTION) {
		this.ACTION = ACTION;
	}

	/**
	 * @param outboundUSSDMessageRequest the outboundUSSDMessageRequest to set
	 */
	public void setOutboundUSSDMessageRequest(
			OutboundUSSDMessageRequest outboundUSSDMessageRequest) {
		this.outboundUSSDMessageRequest = outboundUSSDMessageRequest;
	}
}
