package com.dialog.mife.ussd.dto;

/**
 * Created by Malinda_07654 on 4/12/2016.
 */
public class USSDCallback {
    String callbackData;
    String notifyURL;

    public String getCallbackData() {
        return callbackData;
    }

    public void setCallbackData(String callbackData) {
        this.callbackData = callbackData;
    }

    public String getNotifyURL() {
        return notifyURL;
    }

    public void setNotifyURL(String notifyURL) {
        this.notifyURL = notifyURL;
    }
}
