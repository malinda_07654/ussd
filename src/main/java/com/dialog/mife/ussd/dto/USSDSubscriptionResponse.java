package com.dialog.mife.ussd.dto;

/**
 * Created by Malinda_07654 on 4/12/2016.
 */
public class USSDSubscriptionResponse extends USSDSubscription {
    String resourceURL;

    public String getResourceURL() {
        return resourceURL;
    }

    public void setResourceURL(String resourceURL) {
        this.resourceURL = resourceURL;
    }
}
