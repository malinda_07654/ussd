package com.dialog.mife.ussd.dto;

/**
 * Created by Malinda_07654 on 4/12/2016.
 */
public class USSDSubscription
{
    String shortCode;
    String keyword;
    String clientCorrelator;
USSDCallback callbackReference;

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getClientCorrelator() {
        return clientCorrelator;
    }

    public void setClientCorrelator(String clientCorrelator) {
        this.clientCorrelator = clientCorrelator;
    }

    public USSDCallback getCallbackReference() {
        return callbackReference;
    }

    public void setCallbackReference(USSDCallback callbackReference) {
        this.callbackReference = callbackReference;
    }
}
