package com.dialog.mife.ussd.dto;

/**
 * Created by Malinda_07654 on 4/12/2016.
 */
public class USSDSubscriptionWrap {
    USSDSubscription subscription;

    public USSDSubscription getSubscription() {
        return subscription;
    }

    public void setSubscription(USSDSubscription subscription) {
        this.subscription = subscription;
    }
}
